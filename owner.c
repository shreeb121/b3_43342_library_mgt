#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"
#include "date.h"

void owner_area(user_t *u) {
	int choice;
	do {
		printf("\n\n0. Sign Out\n1. Appoint Librarian\n2. Edit Profile\n3. Change Password\n4. Fees/Fine Report\n5. Book Availability\n6. Book Categories/Subjects\nEnter choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1: // Appoint Librarian
				appoint_librarian();
				break;
			case 2://Edit profile
                user_profile_edit(u);
				break;
			case 3://Change password
                change_password(*u);
				break;
			case 4:
				break;
			case 5://Book Availability
				bookcopy_checkavail();
				break;
			case 6:
				break;
		}
	}while (choice != 0);	
}
void appoint_librarian() {
	// input librarian info
	user_t u;
	user_accept(&u);

	// assign user role as librarian
	strcpy(u.role, ROLE_LIBRARIAN);
	
    // add librarian into the users file
	user_add(&u);
}





